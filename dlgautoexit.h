// ****************************************************************************
//  Project:        GUYMAGER
// ****************************************************************************
//  Programmer:     Guy Voncken
//                  Police Grand-Ducale
//                  Service de Police Judiciaire
//                  Section Nouvelles Technologies
// ****************************************************************************
//  Module:         Auto exit dialog
// ****************************************************************************

// Copyright 2008, 2009, 2010, 2011, 2012, 2013 Guy Voncken
//
// This file is part of guymager.
//
// guymager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// guymager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with guymager. If not, see <http://www.gnu.org/licenses/>.

#ifndef __DLGAUTOEXIT_H__
#define __DLGAUTOEXIT_H__

#include <QtGui> //lint !e537 Repeated include

#ifndef __COMMON_H__
   #include "common.h"
#endif

class t_DlgAutoExitLocal;

class t_DlgAutoExit: public QDialog
{
   Q_OBJECT

   public:
      t_DlgAutoExit ();
      t_DlgAutoExit (QWidget *pParent=NULL, Qt::WFlags Flags=0);
     ~t_DlgAutoExit ();

      static APIRET Show (bool *pAutoExit);

   private slots:
      void SlotCountdown (void);

   private:
      t_DlgAutoExitLocal *pOwn;
};

enum
{
   ERROR_DLGAUTOEXIT_CONSTRUCTOR_NOT_SUPPORTED = ERROR_BASE_DLGAUTOEXIT + 1,
};

#endif

