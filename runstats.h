// ****************************************************************************
//  Project:        GUYMAGER
// ****************************************************************************
//  Programmer:     Guy Voncken
//                  Police Grand-Ducale
//                  Service de Police Judiciaire
//                  Section Nouvelles Technologies
// ****************************************************************************
//  Module:         RunStats provides information about Guymager's state to
//                  others
// ****************************************************************************

// Copyright 2008, 2009, 2010, 2011, 2012, 2013, 2013 Guy Voncken
//
// This file is part of guymager.
//
// guymager is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 2 of the License, or
// (at your option) any later version.
//
// guymager is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with guymager. If not, see <http://www.gnu.org/licenses/>.


class t_RunStatsLocal;

class t_RunStats
{
   public:
      t_RunStats (t_pDeviceList pDeviceList);
     ~t_RunStats (void);

     bool IsConfigured (void) const;
     void Update       (bool LastUpdateOnExit=false);

   private:
      void ReadTemplate (bool LastUpdateOnExit);

   private:
      t_RunStatsLocal *pOwn;
};

typedef t_RunStats *t_pRunStats;
